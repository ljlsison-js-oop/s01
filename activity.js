console.log("hello world")

let students = [
    "Tony",
    "Peter",
    "Wanda",
    "Vision",
    "Loki"
]

// ==========
// =Activity=
// ==========
// 1 How do you create arrays in JS?
let arr = []
// 2 How do you access the first character of an array?
arr[0]
// 3 How do you access the last character of an array?
arr[arr.length-1]
// 4 What array method searches for, and returns the index of a given value in an array? 
// This method returns -1 if given value is not found in the array.
indexOf()
// 5 What array method loops over all elements of an array, performing a user-defined function on each iteration?
forEach()
// 6.   What array method creates a new array with elements obtained from a user-defined function?
map()
// 7.   What array method checks if all its elements satisfy a given condition?
every()
// 8.   What array method checks if at least one of its elements satisfies a given condition?
some()
// 9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
false
// 10.   True or False: array.slice() copies elements from original array and returns these as a new array.
true

// Function Coding:
/* 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing. */
function addToEnd(students, name){
    if(typeof(name) !== "string") return "error - can only add strings to an array"
    else students.push(name)
    return students
}

/* 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing. 
 */
function addToStart(students, name){
    if(typeof(name) !== "string") return "error - can only add strings to an array"
    else students.unshift(name)
    return students
}

/* 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing. */
function elementChecker(array,element){
    if(array.length === 0) return "error - array must not be empty"
    (array.indexOf(element) === -1) ? false : true
}


/* 4.    Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:
if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.
Use the students array and the character "e" as arguments when testing. */
function checkAllStringsEnding(array,char){
    if(array.length === 0) return "error = array must not be empty"
    if(array.some(elem => typeof(elem) !== "string")) return "error - all array elements must be strings"
    if(char.length !== 1) return "error - 2nd argument must be a single character"
    if(typeof(char) !== "string") return "error - 2nd argument must be of data type string"
    return array.every(elem => elem.endsWith(char))
}

/* 5.    Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test. */
function stringLengthSorter(array){
    if(array.some(elem => typeof(elem) !== "string")) return "error - all array elements must be strings"
    else return array.sort((a,b) => a.length - b.length)
}

/* 6.    Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:
if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive
Use the students array and the character "J" as arguments when testing. */
function startsWithCounter(array,char){
    
    let counter = 0

    if(array.length === 0) return "error = array must not be empty"
    if(array.some(elem => typeof(elem) !== "string")) return "error - all array elements must be strings"
    if(char.length !== 1) return "error - 2nd argument must be a single character"
    if(typeof(char) !== "string") return "error - 2nd argument must be of data type string"
    array.map(elem => {
        if(elem.toLowerCase().startsWith(char.toLowerCase())) counter += 1
    })
    return counter

}

/* 7.    Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:
if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
return a new array containing all elements of the array argument that have the string argument in it, must be case-insensitive
Use the students array and the character "jo" as arguments when testing. */
function likeFinder(array,char){
    if(array.length === 0) return "error = array must not be empty"
    if(array.some(elem => typeof(elem) !== "string")) return "error - all array elements must be strings"
    if(typeof(char) !== "string") return "error - 2nd argument must be of data type string"

    let like = []

    array.forEach(elem => {
        if(elem.startsWith(char)) like.push(elem)
    })

    return like
}

/* 8.    Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing. */
function randomPicker(array){
    return array[Math.round(Math.random()*(array.length-1))]
}